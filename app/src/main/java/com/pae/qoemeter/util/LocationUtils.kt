package com.pae.qoemeter.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices

class LocationUtils {
    companion object {
        fun getLastLocation(
            activity: Context,
            onSuccess: (Location) -> Unit,
            onError: (Exception?) -> Unit
        ) {
            if (ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                val locationClient = LocationServices.getFusedLocationProviderClient(activity)

                locationClient.lastLocation.addOnSuccessListener { location ->
                    if (location != null) onSuccess(location) else onError(null)
                }.addOnFailureListener { e ->
                    onError(e)
                }
            } else {
                onError(null)
            }
        }
    }
}