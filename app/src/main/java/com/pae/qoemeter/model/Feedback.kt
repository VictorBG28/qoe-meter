package com.pae.qoemeter.model

data class Feedback(
    var videoFullWatched: Boolean?,
    var reason: String?,
    var streamingApp: String,
    var rating: Int
) {
    constructor() : this(null, null, "", 1)

    fun showSelector(): Boolean {
        return videoFullWatched != null && videoFullWatched == false
    }
}

