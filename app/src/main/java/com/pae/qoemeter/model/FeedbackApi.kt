package com.pae.qoemeter.model

data class FeedbackApi(
    val videoFullWatched: Boolean,
    val reason: String?,
    val streamingApp: String,
    val rating: Int,
    val memUsage: Long,
    val networkType: String,
    val timestamp: Long,
    val deviceName: String,
    val radioFirmware: String,
    val connectionSpeed: Int,
    val latency: Float,
    val packetLoss: String,
    val connectionStrength: Int,
    val location: LocationApi
) {
    override fun toString(): String {
        return "[\nvideoFullWatched = $videoFullWatched" +
                "\nreason = $reason" +
                "\nstreamingApp = $streamingApp" +
                "\nrating = $rating" +
                "\nmemUsage = $memUsage" +
                "\nnetworkType = $networkType" +
                "\ntimestamp = $timestamp" +
                "\ndeviceName = $deviceName" +
                "\nradioFirmware = $radioFirmware" +
                "\nconnectionSpeed = $connectionSpeed" +
                "\nlatency = $latency" +
                "\npacketLoss = $packetLoss" +
                "\nconnectionStrength = $connectionStrength" +
                "\nlocation = $location]"
    }
}