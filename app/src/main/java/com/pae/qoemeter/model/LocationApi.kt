package com.pae.qoemeter.model

import android.location.Location

data class LocationApi(var latitude: Double, var longitude: Double, var altitude: Double) {
    override fun toString(): String {
        return "[\n\tlatitude = $latitude " +
                "\n\tlongitude = $longitude " +
                "\n\taltitude = $altitude\n]"
    }
}

fun fromAndroidLocation(location: Location?): LocationApi {
    if (location != null) {
        return LocationApi(location.latitude, location.longitude, location.altitude)
    }
    return LocationApi(0.0, 0.0, 0.0)
}