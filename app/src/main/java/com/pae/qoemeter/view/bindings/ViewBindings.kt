package com.pae.qoemeter.view.bindings

import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter

@BindingAdapter("app:srcc")
fun srcc(imageView: ImageView, i: Int) {
    imageView.setImageDrawable(ContextCompat.getDrawable(imageView.context, i))
}