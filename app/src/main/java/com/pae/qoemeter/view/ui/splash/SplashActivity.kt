package com.pae.qoemeter.view.ui.splash

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import com.pae.qoemeter.view.ui.feedback.FeedbackActivity
import com.pae.qoemeter.view.ui.home.HomeActivity
import com.pae.qoemeter.view.ui.intro.MainIntroActivity

class SplashActivity : AppCompatActivity() {

    private lateinit var prefs: SharedPreferences;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)

        if (!prefs.getBoolean("intro", false)) {
            startActivityForResult(Intent(this, MainIntroActivity::class.java), 56)
        } else {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 56) {
            if (resultCode == Activity.RESULT_OK) {
                prefs.edit().putBoolean("intro", true).apply()
                startActivity(Intent(this, HomeActivity::class.java))
            }
            finish()
        }
    }
}