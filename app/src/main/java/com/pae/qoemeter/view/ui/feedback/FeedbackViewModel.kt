package com.pae.qoemeter.view.ui.feedback

import android.content.Context
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pae.qoemeter.R
import com.pae.qoemeter.data.repository.FeedbackRepository
import com.pae.qoemeter.model.Feedback
import com.pae.qoemeter.model.FeedbackApi
import com.pae.qoemeter.model.fromAndroidLocation
import com.pae.qoemeter.util.Event
import com.pae.qoemeter.util.LocationUtils
import com.pae.qoemeter.util.forceRefresh
import com.pae.qoemeter.view.utils.StreamingApp
import kotlinx.coroutines.*
import javax.inject.Inject

class FeedbackViewModel @Inject constructor(
    val feedbackRepository: FeedbackRepository,
    val context: Context
) :
    ViewModel() {

    private val _feedback = MutableLiveData<Feedback>()
    private val _showProgress = MutableLiveData<Event<Boolean>>()
    private val _finish = MutableLiveData<Event<Boolean>>()
    private val _feedbackApi = MutableLiveData<FeedbackApi?>()
    private val _message = MutableLiveData<Int?>()

    init {
        _feedback.value = Feedback()
        _feedbackApi.value = null
    }

    val feedback: LiveData<Feedback>
        get() = _feedback

    val showProgress: LiveData<Event<Boolean>>
        get() = _showProgress

    val finish: LiveData<Event<Boolean>>
        get() = _finish

    val feedbackApi: LiveData<FeedbackApi?>
        get() = _feedbackApi

    val message: LiveData<Int?>
        get() = _message

    private var job: Job? = null


    fun setAppSelected(app: StreamingApp) {

        _feedback.value?.streamingApp =
            if (_feedback.value?.streamingApp == app.appName) "" else app.appName
        _feedback.forceRefresh()
    }

    fun setRating(rating: Int) {
        _feedback.value?.rating = rating
    }

    fun setVideoFullWatched(boolean: Boolean) {
        _feedback.value?.videoFullWatched = boolean
        _feedback.forceRefresh()
    }

    fun sendFeedback() {
        if (_feedback.value?.videoFullWatched == null) {
            viewModelScope.launch {
                _message.value = R.string.first_question_not_answered
                delay(1000)
                _message.value = null
            }
            return
        }

        _showProgress.value = Event(true)

        LocationUtils.getLastLocation(context, {
            sendFeedback(it)
        }, {
            sendFeedback(null)
        })
    }

    private fun sendFeedback(loc: Location?) {
        val start = System.currentTimeMillis()
        job = viewModelScope.launch(Dispatchers.IO) {
            val successful =
                feedbackRepository.sendFeedback(_feedback.value!!, fromAndroidLocation(loc))

            ensureActive()
            //Prevent flashing of the dialog
            val actual = System.currentTimeMillis()
            if (actual - start < 2000) {
                delay(2000 - (actual - start))
            }

            // Dismiss progress dialog
            _showProgress.postValue(Event(false))

            ensureActive()
            // Show info in debug
//            delay(250)
//            _feedbackApi.value = feedbackApi

            ensureActive()

            _message.postValue(if (successful) R.string.feedback_sent else R.string.feedback_not_sent)
            delay(2000)
            _message.postValue(null)
            _finish.postValue(Event(true))

        }
    }

    fun cancelSendFeedback() {
        job?.cancel()
        _showProgress.value = Event(false)
    }
}