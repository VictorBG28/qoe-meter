package com.pae.qoemeter.view.ui.intro

import android.Manifest
import android.os.Bundle
import android.view.Gravity
import androidx.viewpager.widget.ViewPager
import com.heinrichreimersoftware.materialintro.app.IntroActivity
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide
import com.pae.qoemeter.R

class MainIntroActivity : IntroActivity() {
    lateinit var simpleSlideFragment: SimpleSlide.SimpleSlideFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        isFullscreen = true
        super.onCreate(savedInstanceState)

        addSlide(
            SimpleSlide.Builder()
                .title(R.string.app_name)
                .description(getString(R.string.slide_main_description))
                .background(R.color.primary)
                .image(R.drawable.ic_foreground)
                .scrollable(false)
                .build()
        )


        val simpleSlide: SimpleSlide = SimpleSlide.Builder()
            .title(getString(R.string.slide_permission_title))
            .description(R.string.slide_permission_description)
            .background(R.color.primary)
            .scrollable(false)
            .image(R.drawable.ic_permissions_image)
            .permissions(
                arrayOf(
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            )
            .build()

        simpleSlideFragment = simpleSlide.fragment as SimpleSlide.SimpleSlideFragment

        addSlide(simpleSlide)

        addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 1) {
                    simpleSlideFragment.descriptionView?.gravity = Gravity.LEFT
                }
            }

        })
    }
}