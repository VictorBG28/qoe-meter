package com.pae.qoemeter.view.ui.home

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.databinding.DataBindingUtil
import com.pae.qoemeter.R
import com.pae.qoemeter.databinding.ActivityHomeBinding
import com.pae.qoemeter.view.ui.feedback.FeedbackActivity
import com.pae.qoemeter.view.ui.store.StoreActivity
import dagger.android.support.DaggerAppCompatActivity

class HomeActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    private lateinit var prefs: SharedPreferences;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        prefs = PreferenceManager.getDefaultSharedPreferences(this)

        binding.storeButton.setOnClickListener {
            startActivity(Intent(this@HomeActivity, StoreActivity::class.java))
        }

        binding.shareButton.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text))
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        binding.fab.setOnClickListener {
            startActivity(Intent(this@HomeActivity, FeedbackActivity::class.java))
        }

    }

    override fun onResume() {
        super.onResume()

        binding.points.text = prefs.getInt("UserPoints", 0).toString()
    }
}