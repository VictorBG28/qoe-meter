package com.pae.qoemeter.view.ui.feedback.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.pae.qoemeter.R
import com.pae.qoemeter.databinding.AppItemBinding
import com.pae.qoemeter.view.utils.StreamingApp

class StreamingAppItem(val item: StreamingApp, private val selected: Boolean) :
    AbstractBindingItem<AppItemBinding>() {

    override val type: Int
        get() = R.id.fastadapter_item

    override fun bindView(binding: AppItemBinding, payloads: List<Any>) {
        binding.item = item
        binding.selected = selected
    }

    override var identifier: Long
        get() = (item.appName + if (selected) "yes" else "no").hashCode().toLong()
        set(value) {}

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): AppItemBinding {
        return AppItemBinding.inflate(inflater, parent, false)
    }
}