package com.pae.qoemeter.view.ui.store

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.pae.qoemeter.R
import com.pae.qoemeter.databinding.ActivityStoreBinding
import dagger.android.support.DaggerAppCompatActivity

class StoreActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityStoreBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_store)

        binding.backButton.setOnClickListener {
            finishAfterTransition()
        }

    }
}