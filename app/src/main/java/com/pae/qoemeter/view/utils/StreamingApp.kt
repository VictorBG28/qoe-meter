package com.pae.qoemeter.view.utils

import androidx.annotation.DrawableRes
import com.pae.qoemeter.R

enum class StreamingApp(val appName: String, @DrawableRes val icon: Int) {
    NETFLIX("Netflix", R.drawable.netflix),
    YOUTUBE("Youtube", R.drawable.youtube),
    DISNEY("Disney+", R.drawable.disney),
    HBO("HBO", R.drawable.hbo),
    PRIMEVIDEO("Prime Video", R.drawable.prime_video),
    TWITCH("Twitch", R.drawable.twitch),
}