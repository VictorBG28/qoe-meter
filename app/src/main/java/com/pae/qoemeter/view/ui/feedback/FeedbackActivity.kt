package com.pae.qoemeter.view.ui.feedback

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.afollestad.assent.Permission
import com.afollestad.assent.runWithPermissions
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.binding.BindingViewHolder
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.pae.qoemeter.R
import com.pae.qoemeter.databinding.ActivityMainBinding
import com.pae.qoemeter.databinding.AppItemBinding
import com.pae.qoemeter.model.Feedback
import com.pae.qoemeter.model.FeedbackApi
import com.pae.qoemeter.util.EventObserver
import com.pae.qoemeter.view.ui.feedback.item.StreamingAppItem
import com.pae.qoemeter.view.utils.StreamingApp
import com.xw.repo.BubbleSeekBar
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class FeedbackActivity : DaggerAppCompatActivity() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: FeedbackViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(FeedbackViewModel::class.java)
    }

    private lateinit var binding: ActivityMainBinding

    private val itemAdapter = ItemAdapter<StreamingAppItem>()

    private var dialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel;

        binding.backButton.setOnClickListener {
            finishAfterTransition()
        }

        binding.scrollView.setOnScrollChangeListener { _, _, _, _, _ ->
            binding.ratingSeekBar.correctOffsetWhenContainerOnScrolling()
        }

        val fastAdapter = FastAdapter.with(itemAdapter)
        binding.recyclerView.apply {
            layoutManager =
                LinearLayoutManager(this@FeedbackActivity, RecyclerView.HORIZONTAL, false)
            adapter = fastAdapter
        }

        fastAdapter.addEventHook(object : ClickEventHook<StreamingAppItem>() {
            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.asBinding<AppItemBinding> {
                    it.card
                }
            }

            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<StreamingAppItem>,
                item: StreamingAppItem
            ) {
                viewModel.setAppSelected(item.item)
            }
        })

        binding.toggleButton.addOnButtonCheckedListener { _, checkedId, isChecked ->
            viewModel.setVideoFullWatched(checkedId == R.id.yes_button && isChecked)
        }

        binding.ratingSeekBar.onProgressChangedListener =
            object : BubbleSeekBar.OnProgressChangedListener {
                override fun onProgressChanged(
                    bubbleSeekBar: BubbleSeekBar?,
                    progress: Int,
                    progressFloat: Float,
                    fromUser: Boolean
                ) {
                    viewModel.setRating(progress)
                }

                override fun getProgressOnActionUp(
                    bubbleSeekBar: BubbleSeekBar?,
                    progress: Int,
                    progressFloat: Float
                ) {
                }

                override fun getProgressOnFinally(
                    bubbleSeekBar: BubbleSeekBar?,
                    progress: Int,
                    progressFloat: Float,
                    fromUser: Boolean
                ) {
                }

            }

        binding.fab.setOnClickListener {
            runWithPermissions(
                Permission.READ_PHONE_STATE,
                Permission.ACCESS_FINE_LOCATION
            ) {
                viewModel.sendFeedback()
            }
        }

        val items = listOf(
            getString(R.string.reason_1),
            getString(R.string.reason_2),
            getString(R.string.reason_3),
            getString(R.string.reason_4),
            getString(R.string.reason_5),
            getString(R.string.other)
        )
        val adapter = ArrayAdapter(this, R.layout.list_item, items)
        (binding.whyNotField.editText as? AutoCompleteTextView)?.setAdapter(adapter)

        viewModel.feedback.observe(this, Observer { populateApps(it!!) })
        viewModel.showProgress.observe(this, EventObserver { showProgress(it) })
        viewModel.feedbackApi.observe(this, Observer { showFeedbackApi(it) })
        viewModel.message.observe(
            this,
            Observer { it?.let { Snackbar.make(binding.root, it, Snackbar.LENGTH_SHORT).show() } })
        viewModel.message.observe(
            this,
            Observer { it?.let { Snackbar.make(binding.root, it, Snackbar.LENGTH_SHORT).show() } })
        viewModel.finish.observe(this, EventObserver { if (it) finishAfterTransition() })
    }

    private fun populateApps(feedback: Feedback) {
        val items =
            StreamingApp.values().map { StreamingAppItem(it, it.appName == feedback.streamingApp) }
        val diffs: DiffUtil.DiffResult = FastAdapterDiffUtil.calculateDiff(itemAdapter, items)
        FastAdapterDiffUtil[itemAdapter] = diffs
    }

    private fun showProgress(showProgress: Boolean) {
        if (showProgress) {
            dialog = MaterialDialog(this).show {
                title(text = getString(R.string.progress_dialog_title))
                customView(viewRes = R.layout.progress_view_dialog)
                negativeButton(text = getString(R.string.cancel)) { dialog ->
                    dialog.dismiss()
                    viewModel.cancelSendFeedback()
                }
                cancelable(false)
                cancelOnTouchOutside(false)
            }
            dialog?.show()
        } else {
            dialog?.dismiss()
        }
    }

    private fun showFeedbackApi(feedbackApi: FeedbackApi?) {
        feedbackApi?.let {
            MaterialDialog(this).show {
                title(text = getString(R.string.feedback_collected))
                message(text = feedbackApi.toString())
                positiveButton(text = getString(R.string.ok)) { dialog ->
                    dialog.dismiss()
                    viewModel.cancelSendFeedback()
                }
            }.show()
        }
    }

    inline fun <reified T : ViewBinding> RecyclerView.ViewHolder.asBinding(block: (T) -> View): View? {
        return if (this is BindingViewHolder<*> && this.binding is T) {
            block(this.binding as T)
        } else {
            null
        }
    }
}