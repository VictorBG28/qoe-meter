package com.pae.qoemeter.data.work

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.pae.qoemeter.QoEApp
import java.util.concurrent.TimeUnit

fun scheduleWork(context: QoEApp) {
    val work = PeriodicWorkRequestBuilder<NotificationWorker>(8, TimeUnit.HOURS, 1, TimeUnit.HOURS)
        .build()
    WorkManager.getInstance(context)
        .enqueueUniquePeriodicWork("NotificationWorker", ExistingPeriodicWorkPolicy.KEEP, work)
}