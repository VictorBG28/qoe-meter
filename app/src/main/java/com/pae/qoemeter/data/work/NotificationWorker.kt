package com.pae.qoemeter.data.work

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.pae.qoemeter.R
import com.pae.qoemeter.view.ui.feedback.FeedbackActivity
import io.karn.notify.Notify

class NotificationWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    override fun doWork(): Result {
        Notify.with(applicationContext)
            .meta {
                clickIntent = PendingIntent.getActivity(
                    applicationContext,
                    0,
                    Intent(applicationContext, FeedbackActivity::class.java),
                    0
                )
            }
            .content {
                title = applicationContext.getString(R.string.title_request_feedback_notification)
                text = applicationContext.getString(R.string.description_request_feedback_notification)
            }
            .header {
                icon = R.drawable.ic_notification
                color = 0x0F6475
            }
            .show()
        return Result.success()
    }

}