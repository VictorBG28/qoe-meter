package com.pae.qoemeter.data.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiManager {
    companion object {
        const val PROD_URL = "http://3.81.167.49:8080/"
    }

    /**
     * Creates an API service of type [service] and adds interceptors to
     * add necessary data to the headers
     */
    fun <T> createApi(service: Class<T>): T {
        val client: OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .writeTimeout(1, TimeUnit.MINUTES)
            .readTimeout(5, TimeUnit.MINUTES)
            .build()

        val retrofit = Retrofit.Builder()
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(PROD_URL)
            .client(client)
            .build()

        return retrofit.create(service)
    }
}