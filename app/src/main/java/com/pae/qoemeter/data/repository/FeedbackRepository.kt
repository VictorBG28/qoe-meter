package com.pae.qoemeter.data.repository

import android.Manifest
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.preference.PreferenceManager
import android.telephony.*
import androidx.core.app.ActivityCompat
import com.pae.qoemeter.data.network.ApiService
import com.pae.qoemeter.model.Feedback
import com.pae.qoemeter.model.FeedbackApi
import com.pae.qoemeter.model.LocationApi
import fr.bmartel.speedtest.SpeedTestReport
import fr.bmartel.speedtest.SpeedTestSocket
import fr.bmartel.speedtest.inter.ISpeedTestListener
import fr.bmartel.speedtest.model.SpeedTestError
import timber.log.Timber
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.concurrent.CountDownLatch
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class FeedbackRepository @Inject constructor(val context: Context, val api: ApiService) {

    suspend fun sendFeedback(feedback: Feedback, locationApi: LocationApi): Boolean {
        val speed = speedTest().toInt()
        val connectionStatistics = connectionStatistics()
        val feedbackApi =
            FeedbackApi(
                videoFullWatched = feedback.videoFullWatched!!,
                reason = feedback.reason,
                streamingApp = feedback.streamingApp,
                rating = feedback.rating,
                memUsage = getMemUsage(),
                networkType = getNetworkType(),
                timestamp = System.currentTimeMillis(),
                deviceName = getDeviceNameAndOs(),
                radioFirmware = getRadioFirmware(),
                connectionSpeed = speed,
                latency = connectionStatistics[0].toFloat(),
                packetLoss = connectionStatistics[1],
                connectionStrength = getDbm(),
                location = locationApi
            )

        Timber.d(feedbackApi.toString())
        val response = api.uploadFeedback(feedbackApi)

        Timber.d("Upload feedback successful?: ${response.isSuccessful}")
        if (response.isSuccessful) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().putInt("UserPoints", prefs.getInt("UserPoints", 0) + 1000).apply()
        }

        return response.isSuccessful
    }

    private fun getDbm(): Int {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val telephonyManager =
                context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            return getSignalStrengthDbm(telephonyManager.allCellInfo[0])
        }
        return 0
    }

    private fun getSignalStrengthDbm(cellInfo: CellInfo): Int {
        if (cellInfo is CellInfoCdma) {
            return cellInfo.cellSignalStrength.dbm
        }
        if (cellInfo is CellInfoGsm) {
            return cellInfo.cellSignalStrength.dbm
        }
        if (cellInfo is CellInfoLte) {
            return cellInfo.cellSignalStrength.dbm
        }
        return if (cellInfo is CellInfoWcdma) {
            cellInfo.cellSignalStrength.dbm
        } else {
            0
        }
    }

    private fun speedTest(): Double {
        var result: Double? = 0.0
        val latch = CountDownLatch(1)
        val socket = SpeedTestSocket()
        socket.addSpeedTestListener(object : ISpeedTestListener {
            override fun onCompletion(report: SpeedTestReport?) {
                result = report?.transferRateBit?.toDouble()
                latch.countDown()
            }

            override fun onProgress(percent: Float, report: SpeedTestReport?) {
            }

            override fun onError(speedTestError: SpeedTestError?, errorMessage: String?) {
            }

        })
        socket.startFixedDownload("http://ipv4.ikoula.testdebit.info/1M.iso", 2000)
        latch.await()
        return result ?: 0.0
    }

    private fun connectionStatistics(): List<String> {
        var latency = ""
        var packetLoss = ""
        val ip = "172.217.17.3" // google.es
        val pingCmd = "ping -c 10 $ip"
        try {
            val r = Runtime.getRuntime()
            val p = r.exec(pingCmd)
            val inn = BufferedReader(InputStreamReader(p.inputStream))
            var inputLine: String?
            var packetLossResult: String? = null
            var latencyResult: String? = null
            while (inn.readLine().also { inputLine = it } != null) {
                latencyResult = inputLine
                if (inputLine!!.contains("packet loss")) {
                    packetLossResult = inputLine
                }
            }
            val keyValue =
                latencyResult!!.split("=".toRegex()).toTypedArray()
            val value =
                keyValue[1].split("/".toRegex()).toTypedArray()
            latency = value[1]

            packetLoss = packetLossResult!!.split(",".toRegex())[2].split(" ".toRegex())[1]

        } catch (e: Exception) {
            Timber.e(e)
        }
        return listOf(latency, packetLoss)
    }

    @SuppressLint("MissingPermission")
    private fun getNetworkType(): String {
        val connectivityManager =
            context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val nw = connectivityManager.activeNetwork ?: return "-"
        val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return "-"
        when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> return "WIFI"
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> return "ETHERNET"
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                when (tm.dataNetworkType) {
                    TelephonyManager.NETWORK_TYPE_GPRS,
                    TelephonyManager.NETWORK_TYPE_EDGE,
                    TelephonyManager.NETWORK_TYPE_CDMA,
                    TelephonyManager.NETWORK_TYPE_1xRTT,
                    TelephonyManager.NETWORK_TYPE_IDEN,
                    TelephonyManager.NETWORK_TYPE_GSM -> return "2G"
                    TelephonyManager.NETWORK_TYPE_UMTS,
                    TelephonyManager.NETWORK_TYPE_EVDO_0,
                    TelephonyManager.NETWORK_TYPE_EVDO_A,
                    TelephonyManager.NETWORK_TYPE_HSDPA,
                    TelephonyManager.NETWORK_TYPE_HSUPA,
                    TelephonyManager.NETWORK_TYPE_HSPA,
                    TelephonyManager.NETWORK_TYPE_EVDO_B,
                    TelephonyManager.NETWORK_TYPE_EHRPD,
                    TelephonyManager.NETWORK_TYPE_HSPAP,
                    TelephonyManager.NETWORK_TYPE_TD_SCDMA -> return "3G"
                    TelephonyManager.NETWORK_TYPE_LTE,
                    TelephonyManager.NETWORK_TYPE_IWLAN, 19 -> return "4G"
                    TelephonyManager.NETWORK_TYPE_NR -> return "5G"
                    else -> return "?"
                }
            }
            else -> return "?"
        }
    }

    private fun getMemUsage(): Long {
        val mi = ActivityManager.MemoryInfo()
        val activityManager = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager?
        activityManager!!.getMemoryInfo(mi)
        return mi.availMem
    }

    private fun getDeviceNameAndOs(): String {
        return "${android.os.Build.MANUFACTURER} ${android.os.Build.MODEL}; Android ${android.os.Build.VERSION.RELEASE};"
    }

    private fun getRadioFirmware(): String {
        return android.os.Build.getRadioVersion() ?: ""
    }
}