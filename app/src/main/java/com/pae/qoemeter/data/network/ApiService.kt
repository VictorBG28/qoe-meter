package com.pae.qoemeter.data.network

import com.pae.qoemeter.model.FeedbackApi
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST("feedback")
    suspend fun uploadFeedback(@Body body: FeedbackApi): Response<Unit>
}