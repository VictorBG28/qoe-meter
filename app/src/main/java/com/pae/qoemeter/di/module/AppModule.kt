package com.pae.qoemeter.di.module

import android.app.Application
import android.content.Context
import com.pae.qoemeter.QoEApp
import com.pae.qoemeter.data.network.ApiManager
import com.pae.qoemeter.data.network.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun provideApi(): ApiService {
        return ApiManager().createApi(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun providesApplication(app: Application): QoEApp {
        return app as QoEApp
    }

    @Singleton
    @Provides
    fun providesContext(app: Application): Context {
        return app
    }
}