package com.pae.qoemeter.di.component

import android.app.Application
import com.pae.qoemeter.QoEApp
import com.pae.qoemeter.di.module.ActivitiesModule
import com.pae.qoemeter.di.module.AppModule
import com.pae.qoemeter.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Main component of the application and the one injected into the application scope.
 *
 * It contains all the modules the app needs.
 */

@Singleton
@Component(
    modules = [
        AppModule::class,
        AndroidInjectionModule::class,
        ViewModelModule::class,
        ActivitiesModule::class
    ]
)
interface AppComponent : AndroidInjector<QoEApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}