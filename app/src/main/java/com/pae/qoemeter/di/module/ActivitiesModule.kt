package com.pae.qoemeter.di.module

import com.pae.qoemeter.view.ui.feedback.FeedbackActivity
import com.pae.qoemeter.view.ui.home.HomeActivity
import com.pae.qoemeter.view.ui.store.StoreActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Activities modules where all the activities are listed.
 *
 * They are listed as [ContributesAndroidInjector] so they have an injector generated
 * automatically, thus we have not to worry about more that put the new activities here.
 *
 * All dependencies will be auto injected (indicating @Inject to the injected param or constructor)
 */
@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector
    abstract fun contributeFeedbackActivity(): FeedbackActivity

    @ContributesAndroidInjector
    abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun contributeStoreActivity(): StoreActivity
}