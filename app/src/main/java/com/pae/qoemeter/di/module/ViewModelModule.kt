package com.pae.qoemeter.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pae.qoemeter.di.util.ViewModelKey
import com.pae.qoemeter.di.util.ViewModelFactory
import com.pae.qoemeter.view.ui.feedback.FeedbackViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Module that lists all the [ViewModel] available so they can be created by the [ViewModelFactory]
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FeedbackViewModel::class)
    abstract fun feedbackViewModel(viewModel: FeedbackViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}