package com.pae.qoemeter.di.injector

import com.pae.qoemeter.QoEApp
import com.pae.qoemeter.di.component.AppComponent
import com.pae.qoemeter.di.component.DaggerAppComponent

/**
 * Injects the dependencies into the given [app] instance
 */
fun initInjector(app: QoEApp) {
    val appComponent: AppComponent = DaggerAppComponent.builder().application(app).build()
    appComponent.inject(app)
}