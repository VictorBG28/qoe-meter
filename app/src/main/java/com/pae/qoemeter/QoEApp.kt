package com.pae.qoemeter

import android.app.Application
import com.facebook.stetho.Stetho
import com.pae.qoemeter.data.work.scheduleWork
import com.pae.qoemeter.di.injector.initInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class QoEApp : Application(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }

        initInjector(this)
        scheduleWork(this)
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}